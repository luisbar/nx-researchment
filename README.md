# Nx researchment notes

- Scaffold project for React, Angular, Next, etc
- Run task such as lint, test, build, and so on, in parallel
- We can cache the artifacts resulted from running tasks, beside that it allows to cache it locally or distributed using [Nx Cloud](https://nx.app)
  - We can choose which tasks should be cached
  - We can enable cloud/distributed cache in an easy way
- We can include it in our CI pipeline in order to reduce the build time, as you can see in the `.gitlab-ci.yaml`
- In order to Nx detects packages automatically you do not have to add the `workspace.json` file
- Nx acquired Lerna, you can use both together
- Turbo Repo at this moment (17/09/2022) is a sub set of Nx (Turbo Repo is a tool created by Vercel)
- Some parts of the Nx doc is outdated
- Nx not only analyze the `package.json` but also the code in order to find dependencies and it has great interface for checking dependencies
- Nx has the `affected` command which runs a command (e.g. build) only on updated packages

# How to run

1. Select node version
   `nvm use`
2. Install dependencies
   `npm i`
3. Run it
   `npx nx run-many --target=serve`

# How to run the dependency graph

`npx nx graph --watch`
